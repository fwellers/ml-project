#define CL_HPP_ENABLE_EXCEPTIONS
#include <CL/opencl.hpp>
#include "OpenCLWrapper.hpp"

int OpenCLWrapper::setWorkGroupSize()
{
	cl_int status = 0;
	globalSize[0] = width1/4;
	globalSize[1] = height0/4;
	localSize[0] = blockSize;
	localSize[1] = blockSize;
	return 0;
}
OpenCLWrapper::OpenCLWrapper(bool optimized)
{
	init();
	if (optimized) {
		this->program = loadProgram("cl/matrix_vector_multiplication_n_per_product.cl");
	}
}
int OpenCLWrapper::init()
{
	try {
		// create device
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		if (platforms.empty()) {
			std::cerr << "No platforms found!" << std::endl;
			exit(1);
		}
		cl::Platform platform;
		for (auto& p: platforms) {
			std::string platformVersion = p.getInfo<CL_PLATFORM_VERSION>();
			if (platformVersion.find("OpenCL 2.")!=std::string::npos ||
					platformVersion.find("OpenCL 3.")!=std::string::npos) {
				// Note: an OpenCL 3.x platform may not support all required features!
				platform = p;
			}
		}
		if (platform()==nullptr) {
			std::cout << "No OpenCL 2.0 or newer platform found.\n";
			return -1;
		}
		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

		if (devices.empty()) {
			std::cerr << "No devices found!" << std::endl;
			exit(1);
		}
		this->device = devices.front();
		this->context = cl::Context(this->device);
		this->commandQueue = cl::CommandQueue(this->context, this->device);
		return 0;
	}
	catch (cl::Error& error) {
		std::cout << error.what() << " failed with error code " << error.err() << std::endl;
		return 1;
	}
}
cl::Program OpenCLWrapper::loadProgram(const std::string& fileName)
{
	try {
		std::ifstream kernel_file(fileName);
		std::string src(std::istreambuf_iterator<char>(kernel_file), (std::istreambuf_iterator<char>()));

		cl::Program::Sources sources;
		sources.emplace_back(src.c_str(), src.length());
		cl::Program program = cl::Program(context, sources);

		auto err = program.build();
		if (err!=CL_BUILD_SUCCESS) {
			std::cerr << "Error!\nBuild Status: " << program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device)
					  << "\nBuild Log:\t " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
			exit(1);
		}
		else {
			//std::cout << "Built Successfully" << std::endl;
			return program;
		}
	}
	catch (cl::Error& error) {
		std::cout << error.what() << " failed with error code " << error.err() << std::endl;
		exit(1);
	}
}
int OpenCLWrapper::runVectorAdd()
{
	try {
		// load program
		program = loadProgram("cl/vector_addition.cl");

		int ARRAY_DIM = 500000;
		std::vector<float> h_a(ARRAY_DIM);
		std::vector<float> h_b(ARRAY_DIM);
		std::vector<float> h_c(ARRAY_DIM);

		std::random_device rd;
		std::mt19937 g(rd());
		std::uniform_real_distribution<float> dist{-10000.0f, 10000.0f};
		for (int i = 0; i<ARRAY_DIM; i++) {
			h_a[i] = dist(g);
			h_b[i] = dist(g);
		}

		cl::Buffer d_a(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, ARRAY_DIM*sizeof(int),
				h_a.data());
		cl::Buffer d_b(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, ARRAY_DIM*sizeof(int),
				h_b.data());
		cl::Buffer d_c(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, ARRAY_DIM*sizeof(int));

		cl::Kernel kernel(program, "vadd");
		kernel.setArg(0, d_a);
		kernel.setArg(1, d_b);
		kernel.setArg(2, d_c);

		size_t globalWorkGroupSize = ARRAY_DIM/256;
		size_t localWorkGroupSize = 256;

		clock_t start, end;
		double diffOpencl = 0;
		int numExecutions = 400;
		commandQueue = cl::CommandQueue(context, device);
		start = clock();
		for (int i = 0; i<numExecutions; i++) {
			commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(globalWorkGroupSize), cl::NDRange
					(localWorkGroupSize));
			commandQueue.enqueueReadBuffer(d_c, CL_TRUE, 0, ARRAY_DIM*sizeof(int), h_c.data());
			commandQueue.finish();
		}
		end = clock();
		diffOpencl += ((double) (end-start))/CLOCKS_PER_SEC;
		diffOpencl = 10e3*diffOpencl/numExecutions;

		double diffCpu = 0;
		start = clock();
		for (int i = 0; i<numExecutions; i++) {
			for (int j = 0; j<ARRAY_DIM; j++) {
				h_c[j] = h_a[j]+h_b[j];
			}
		}
		end = clock();
		diffCpu += ((double) (end-start))/CLOCKS_PER_SEC;
		diffCpu = 10e3*diffCpu/numExecutions;
		printf("The Array has %i entries.\n", ARRAY_DIM);
		printf("The duration for OpenCL was %f ms.\n", diffOpencl);
		printf("The duration for CPU was %f ms.\n", diffCpu);
		printf("The performance gain is %f percent.\n", (100*(diffCpu-diffOpencl)/diffOpencl));
		return 0;
	}
	catch (cl::Error& error) {
		std::cout << error.what() << " failed with error code " << error.err() << std::endl;
		return 1;
	}
}

std::vector<float> OpenCLWrapper::runMatrixVectorMultiOnePerProduct(const std::vector<float>& matrix, int rows,
		int cols, const
std::vector<float>&
vector)
{
	try {
		// load program
		program = loadProgram("cl/matrix_vector_multiplication_one_per_product.cl");

		std::vector<float> h_a = matrix;
		std::vector<float> h_x = vector;
		std::vector<float> h_c(rows);
		int h_rows = rows;
		int h_cols = cols;

		cl::Buffer d_a(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, h_a.size()*sizeof
						(float),
				h_a.data());
		cl::Buffer d_x(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, h_x.size()*sizeof
						(float),
				h_x.data());
		cl::Buffer d_c(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, h_c.size()*sizeof(float));

		cl::Kernel kernel(program, "matrixVectorMult");
		kernel.setArg(0, d_a);
		kernel.setArg(1, d_x);
		kernel.setArg(2, d_c);
		kernel.setArg(3, h_rows);
		kernel.setArg(4, h_cols);

		size_t localWorkGroupSize = 1;
		size_t globalWorkGroupSize = rows; // Size of rows, since each work item calculates one final value

		int numExecutions = 1; // run only once
		commandQueue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);

		/* do not preheat!
		for (int i = 0; i<1; i++) {
			commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(globalWorkGroupSize), cl::NDRange
					(localWorkGroupSize));
		}
		commandQueue.finish();
		 */

		cl::Event startEvent, endEvent;
		double diffOpencl = 0;
		for (int i = 0; i<numExecutions; i++) {
			commandQueue.enqueueMarkerWithWaitList(nullptr, &startEvent);
			commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(globalWorkGroupSize),
					cl::NDRange(localWorkGroupSize));
			commandQueue.finish();
			commandQueue.enqueueMarkerWithWaitList(nullptr, &endEvent);
			commandQueue.finish();
			diffOpencl += ((cl_double) (endEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>()-
					startEvent.getProfilingInfo<CL_PROFILING_COMMAND_START>()))*1e-06;
		}

		printf("The average duration for OpenCL was %f ms.\n", diffOpencl/numExecutions);
		commandQueue.enqueueReadBuffer(d_c, CL_TRUE, 0, h_c.size()*sizeof(float), h_c.data());
		commandQueue.finish();

		return h_c;
	}
	catch (cl::Error& error) {
		std::cout << error.what() << " failed with error code " << error.err() << std::endl;
		exit(0);
	}
}
std::vector<float> OpenCLWrapper::runMatrixVectorMultiNPerProduct(const std::vector<float>& matrix, int rows, int cols,
		const std::vector<float>& vector)
{
	try {
		// load program, is already default
		if (!optimized) {
			program = loadProgram("cl/matrix_vector_multiplication_n_per_product.cl");
		}

		std::vector<float> h_a = matrix;
		std::vector<float> h_x = vector;
		std::vector<float> h_c(rows);
		int h_rows = rows;
		int h_cols = cols;

		cl::Buffer d_a(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, h_a.size()*sizeof
						(float),
				h_a.data());
		cl::Buffer d_x(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, h_x.size()*sizeof
						(float),
				h_x.data());
		cl::Buffer d_c(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, h_c.size()*sizeof(float));

		size_t localWorkGroupSize[2];
		// lowerValue * higherValue <= 256 (CL_DEVICE_MAX_WORK_GROUP_SIZE)
		size_t lowerValue = 8;
		size_t higherValue = 32;
		if (rows<cols) {
			localWorkGroupSize[0] = lowerValue;
			localWorkGroupSize[1] = higherValue;
		}
		else {
			localWorkGroupSize[0] = higherValue;
			localWorkGroupSize[1] = lowerValue;
		}
		size_t globalWorkGroupSize[2];
		globalWorkGroupSize[0] = rows;
		globalWorkGroupSize[1] = localWorkGroupSize[1];
		cl::Kernel kernel(program, "matrixVectorMult");
		kernel.setArg(0, d_a);
		kernel.setArg(1, d_x);
		kernel.setArg(2, d_c);
		kernel.setArg(3, localWorkGroupSize[0]*localWorkGroupSize[1]*sizeof(float), nullptr);
		kernel.setArg(4, h_rows);
		kernel.setArg(5, h_cols);

		commandQueue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);

		if (!optimized) {
			/* Do not preheat!
			for (int i = 0; i<100; i++) {
				commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange( globalWorkGroupSize/localWorkGroupSize), cl::NDRange
						(localWorkGroupSize, localWorkGroupSize));
			}
			commandQueue.finish();
			 */
			int numExecutions = 1;
			cl::Event startEvent, endEvent;
			double diffOpencl = 0;
			for (int i = 0; i<numExecutions; i++) {
				commandQueue.enqueueMarkerWithWaitList(nullptr, &startEvent);
				commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange
						(globalWorkGroupSize[0], globalWorkGroupSize[1]), cl::NDRange
						(localWorkGroupSize[0], localWorkGroupSize[1]));
				commandQueue.finish();
				commandQueue.enqueueMarkerWithWaitList(nullptr, &endEvent);
				commandQueue.finish();
				diffOpencl += ((cl_double) (endEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>()-
						startEvent.getProfilingInfo<CL_PROFILING_COMMAND_START>()))*1e-06;
			}
			printf("The average duration for OpenCL was %f ms.\n", diffOpencl/numExecutions);
		}
		else {
			commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange
					(globalWorkGroupSize[0], globalWorkGroupSize[1]), cl::NDRange
					(localWorkGroupSize[0], localWorkGroupSize[1]));
		}
		commandQueue.enqueueReadBuffer(d_c, CL_TRUE, 0, h_c.size()*sizeof(float), h_c.data());
		commandQueue.finish();

		return h_c;
	}
	catch (cl::Error& error) {
		std::cout << error.what() << " failed with error code " << error.err() << std::endl;
		exit(0);
	}
}
