//
// Created by finn on 07.03.23.
//

#include "BasicNeuralNetwork.hpp"

#include <utility>

using Eigen::Vector2f;

BasicNeuralNetwork::BasicNeuralNetwork(bool useGpu)
{
	this->useGpu = useGpu;
	if (useGpu) {
		this->openClWrapper = OpenCLWrapper();
	}
	// input layer,28x28 | hidden layer 128 | output layer 10
	MatrixXf weights1 = MatrixXf::Random(128, 28*28);
	weights1 = weights1.unaryExpr([](float val) { return val<0 ? -1*val : val; });
	MatrixXf weights2 = MatrixXf::Random(10, 128);
	weights2 = weights2.unaryExpr([](float val) { return val<0 ? -1*val : val; });
	this->weights.push_back(weights1);
	this->weights.push_back(weights2);
}
MatrixXf BasicNeuralNetwork::forwardPass(const VectorXf& inputVector)
{
	float bias = 0;

	VectorXf a1 = multiplyMatrixVector(this->weights[0], inputVector);
	VectorXf a1bias = a1.unaryExpr([bias](float myVal) { return myVal+bias; });
	//printf("a1\n");
	//std::cout << a1bias << std::endl;
	VectorXf h1 = a1bias.unaryExpr(&MathUtils::reLU);
	//printf("h1\n");
	//std::cout << h1 << std::endl;
	VectorXf a2 = multiplyMatrixVector(this->weights[1], h1);
	VectorXf a2bias = a2.unaryExpr([bias](float myVal) { return myVal+bias; });
	//printf("a2\n");
	//std::cout << a2bias << std::endl;
	VectorXf h2 = a2bias.unaryExpr([&a2bias](float value) { return MathUtils::softMax(value, a2bias); });
	//printf("h2\n");
	//std::cout << h2 << std::endl;

	std::vector<VectorXf> tmpOutputVectors(2);
	tmpOutputVectors[0] = h1;
	tmpOutputVectors[1] = h2;
	this->outputVectors = tmpOutputVectors;

	std::vector<VectorXf> tmpNetworkValues(2);
	tmpNetworkValues[0] = a1bias;
	tmpNetworkValues[1] = a2bias;
	this->networkValues = tmpNetworkValues;
	return h2;
}

MatrixXf BasicNeuralNetwork::oldForwardPass(float value1, float value2)
{
	int sizeA = 2;
	int sizeb = 2;
	MatrixXf weights1 = MatrixXf::Ones(2, 2);
	MatrixXf weights2 = MatrixXf::Ones(1, 2);
	float bias = 0;

	Vector2f myVec;
	myVec[0] = value1;
	myVec[1] = value2;
	VectorXf a1 = multiplyMatrixVector(weights1, myVec);
	VectorXf a1bias = a1.unaryExpr([bias](float myVal) { return myVal+bias; });
	printf("a1\n");
	std::cout << a1bias << std::endl;
	VectorXf h1 = a1bias.unaryExpr(&MathUtils::reLU);
	printf("h1\n");
	std::cout << h1 << std::endl;
	VectorXf a2 = multiplyMatrixVector(weights2, h1);
	VectorXf a2bias = a2.unaryExpr([bias](float myVal) { return myVal+bias; });
	printf("a2\n");
	std::cout << a2bias << std::endl;
	VectorXf h2 = a2bias.unaryExpr(&MathUtils::reLU);
	printf("h2\n");
	std::cout << h2 << std::endl;

	std::vector<MatrixXf> tmpWeights;
	tmpWeights.push_back(weights1);
	tmpWeights.push_back(weights2);
	this->weights = tmpWeights;

	std::vector<VectorXf> tmpOutputVectors;
	tmpOutputVectors.emplace_back(h1.col(0));
	tmpOutputVectors.emplace_back(h2.col(0));
	this->outputVectors = tmpOutputVectors;

	return h2;
}

/**
 * Run online backpropagation.
 *
 * Steps:
 * 1. Calculate error between expected values for given input and actual outputs of output layer.
 * 2. Following the equation, calculate the deltas for each of the previous errors.
 *  -> Equation is based on previous error, weights and derivative of the activation function:
 *  gam_j = phi'(net_j) * Sum(_k=1)^n](w_j_k *gam_k)
 */
int BasicNeuralNetwork::backwardPassOnline(const VectorXf& targetValues)
{
	int layers = 2; // at the moment static
	std::vector<Eigen::VectorXf> tmpErrorVectors(2);
	VectorXf outputLayer = this->outputVectors[layers-1];
	VectorXf initialDeltaI = this->calcInitialErrors(targetValues, outputLayer); // todo: double check if ok
	tmpErrorVectors[layers-1] = VectorXf(initialDeltaI.size());
	for (int i = 0; i<initialDeltaI.size(); i++) {
		tmpErrorVectors[layers-1](i) = initialDeltaI[i];
	}
	update(layers-1, tmpErrorVectors[layers-1]);
	for (int i = layers-2; i>=0; i--) {
		VectorXf currentLayer = this->outputVectors[i];
		VectorXf errorVector = VectorXf(currentLayer.size());
		for (int j = 0; j<currentLayer.size(); j++) {
			float sumCounter = 0;
			MatrixXf currentWeights = this->weights[i];
			for (int k = 0; k<tmpErrorVectors[i+1].size(); k++) {
				sumCounter += currentWeights(j, k)*tmpErrorVectors[i+1](k);
			}
			// gam_j = phi'(net_j) * Sum(_k=1)^n](w_j_k *gam_k)
			auto errorValue_j_k = MathUtils::reLUDeriv(this->networkValues[i][j])*sumCounter;
			errorVector(j) = errorValue_j_k;
		}
		update(i, tmpErrorVectors[i+1]);
		tmpErrorVectors[i] = (errorVector);
	}
	this->errorVectors = std::move(tmpErrorVectors);
	//printf("Error vector 0:\n");
	//std::cout << errorVectors[0] << std::endl;
	//printf("Error vector 1:\n");
	//std::cout << errorVectors[1] << std::endl;
	return 0;
}

void BasicNeuralNetwork::update(int layer, VectorXf errorVector)
{
	auto currentLayer = this->outputVectors[layer];
	for (int i = 0; i<currentLayer.size(); i++) {
		for (int j = 0; j<errorVector.size(); j++) {
			auto error_j = errorVector[j];
			auto delta_w_i_j = -learningRate * currentLayer[i] * errorVector[j];
			this->weights[layer](i,j) = this->weights[layer](i,j) + delta_w_i_j;
		}
	}
}

/**
 * Run batch backpropagation.
 *
 */
MatrixXf BasicNeuralNetwork::backwardPassBatch(VectorXf targetValues)
{
	return MatrixXf();
}
/**
 * Run mini-Batch backpropagation.
 */
MatrixXf BasicNeuralNetwork::backwardPassMiniBatch(VectorXf targetValues)
{
	return MatrixXf();
}
VectorXf BasicNeuralNetwork::calcInitialErrors(VectorXf targetValues, VectorXf outputLayerValues)
{
	VectorXf newVector = std::move(outputLayerValues);
	for (int i = 0; i<targetValues.size(); i++) {
		newVector[i] = newVector[i]-targetValues[i];
	}
	return newVector;
}
VectorXf BasicNeuralNetwork::multiplyMatrixVector(const MatrixXf& matrix, const VectorXf& vector)
{
	if (this->useGpu) {
		std::vector<float> tmpVec(vector.size());
		VectorXf::Map(&tmpVec[0], vector.size()) = vector;
		auto resultVec = this->openClWrapper.runMatrixVectorMultiNPerProduct(MathUtils::flattenMatrix(matrix), matrix
						.rows(),
				matrix
						.cols(), tmpVec);
		return VectorXf::Map(&resultVec[0], resultVec.size());
	}
	else {
		return matrix*vector;
	}
}
void BasicNeuralNetwork::train(std::map<int, std::vector<std::array<unsigned char, 28*28>>> trainingsData)
{
	printf("Training NN with GPU enabled = %b\n", useGpu);
	const int maxEpisodes = 20;
	int episodeCounter = 0;
	float totalDuration = 0;
	clock_t start, end;
	while (episodeCounter<maxEpisodes) {
		start = clock();
		for (int i = 0; i<10; i++) {
			for (int j = 0; j<1000; j++) {
				std::array<unsigned char, 28*28> data = trainingsData[i][j];
				VectorXf inputVector(data.size());
				for (int k = 0; k<(int) data.size(); k++) {
					inputVector(k) = data[k];
				}
				auto val = forwardPass(inputVector);
				VectorXf targetValues = VectorXf::Zero(10);
				targetValues[i] = 1;
				backwardPassOnline(targetValues);
			}
		}
		episodeCounter++;
		end = clock();
		float duration = (float) (end-start)/CLOCKS_PER_SEC;
		totalDuration += duration;
		printf("Episode %i finished after %f seconds\n", episodeCounter, duration);
	}
	printf("The whole training took %f seconds with GPU enabled = %b\n", totalDuration, useGpu);
	std::array<unsigned char, 28*28> data = trainingsData[0][0];
	VectorXf testVector(data.size());
	for (int k = 0; k<(int) data.size(); k++) {
		testVector(k) = data[k];
	}
	auto val = forwardPass(testVector);
	printf("Running test with Vector for 0: \n");
	std::cout << val << std::endl;
}
