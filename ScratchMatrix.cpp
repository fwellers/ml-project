//
// Created by finn on 06.03.23.
//
#include <CL/opencl.hpp>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <random>

cl::Device createDevice()
{
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);

	if (platforms.empty()) {
		std::cerr << "No platforms found!" << std::endl;
		exit(1);
	}

	auto firstPlatform = platforms[1];
	std::vector<cl::Device> devices;
	firstPlatform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

	if (devices.empty()) {
		std::cerr << "No devices found!" << std::endl;
		exit(1);
	}
	return devices.front();
}

cl::Program initDevice(cl::Context& context, const cl::Device& device, const std::string& kernel_file_path)
{
	std::ifstream kernel_file(kernel_file_path);
	std::string src(std::istreambuf_iterator<char>(kernel_file), (std::istreambuf_iterator<char>()));

	cl::Program::Sources sources;
	sources.emplace_back(src.c_str());
	//sources.emplace_back(src.c_str(), src.length());
	cl::Program program = cl::Program(context, sources);

	auto err = program.build();
	if (err!=CL_BUILD_SUCCESS) {
		std::cerr << "Error!\nBuild Status: " << program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device)
				  << "\nBuild Log:\t " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
		exit(1);
	}
	return program;
}

void parMultiplyMatrices(cl::Context& context, const cl::Program& program, const cl::Device& device, int* a,
		int* b,
		int* c,
		const int M,
		const int N,
		const int K)
{

	/**
	 * Create buffers and allocate memory on the device.
	 * */
	cl_int err;

	cl::Buffer aBuf(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, M*K*sizeof(int), a);
	cl::Buffer bBuf(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, K*N*sizeof(int), b);
	cl::Buffer cBuf(context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, M*N*sizeof(int));

	/**
	 * Set kernel arguments.
	 * */

	cl::Kernel kernel(program, "multiplyMatrices");
	kernel.setArg(0, aBuf);
	kernel.setArg(1, bBuf);
	kernel.setArg(2, cBuf);
	kernel.setArg(3, sizeof(unsigned int), &M);
	kernel.setArg(4, sizeof(unsigned int), &N);
	kernel.setArg(5, sizeof(unsigned int), &K);

	/**
	 * Execute the kernel function and collect its result.
	 * */

	cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);
	queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(N, M));
	queue.enqueueReadBuffer(cBuf, CL_TRUE, 0, M*N*sizeof(int), c);
	queue.finish();
}

int matrix_multiply()
{
	cl_int err;
	cl::Context context;

	clock_t start, end;
	const int EXECUTIONS = 40;

	/**
	 * Prepare input constants related to the dimensions of the matrices.
	 * */

	const int M = 1 << 4;
	const int N = 1 << 4;
	const int K = 1 << 12;

	/**
	 * Prepare input matrices A and B.
	 * */

	const size_t ROWS_A = M;
	const size_t COLS_A = K;
	std::vector<int> a(ROWS_A*COLS_A, 3);

	const size_t ROWS_B = K;
	const size_t COLS_B = N;
	std::vector<int> b(ROWS_B*COLS_B, 5);

	/**
	 * Prepare sequential and parallel output matrices.
	 * */

	const size_t ROWS_C = M;
	const size_t COLS_C = N;
	std::vector<int> cs(ROWS_C*COLS_C);
	std::vector<int> cp(ROWS_C*COLS_C);
	cl::Device device = createDevice();

	cl::Program program = initDevice(context, device, "cl/old-matrix_multiplication.cl");

	start = clock();
	for (int i = 0; i<EXECUTIONS; i++) {
		parMultiplyMatrices(context, program, device, a.data(), b.data(), cp.data(), M, N, K);
	}
	end = clock();
	double parTime = ((double) 10e3*(end-start))/CLOCKS_PER_SEC/EXECUTIONS;

	std::cout << "Results: \n\tA[0] = " << a[0] << "\n\tB[0] = " << b[0] << "\n\tC[0] = " << cp[0] << std::endl;
	std::cout << "Mean execution time: ms;\n\tParallel: " << parTime << " ms." << std::endl;

	return 0;
}
