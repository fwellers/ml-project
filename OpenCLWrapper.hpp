#ifndef AMDMLOPENCL_OPENCLWRAPPER_HPP
#define AMDMLOPENCL_OPENCLWRAPPER_HPP
#define CL_HPP_ENABLE_EXCEPTIONS
#ifndef BLOCK_SIZE
#define BLOCK_SIZE 4// default value
#endif

#include <CL/opencl.hpp>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <random>

class OpenCLWrapper {
	size_t globalSize[2]; //global ND range
	size_t localSize[2];  // local work items
	cl_uint blockSize; // Size of block of shared memory

	std::vector<float> h_inputVector0;
	cl_int width0;
	cl_int height0;
	std::vector<float> h_inputVector1;
	cl_int width1;
	cl_int height1;
	std::vector<float> h_outputVector;

	cl::Buffer d_inputBuffer0;
	cl::Buffer d_inputBuffer1;
	cl::Buffer d_outputBuffer;
	cl::CommandQueue commandQueue;
	cl::Context context;
	cl::Device device;
	cl::Program program;
	cl::Program matrixVectorMultiplication;
	cl::Kernel matrixVectorMultiplicationKernel;
	cl_int n; // n height of matrix A and width of matrix B
	cl_int m; // m width of matrix A
	cl_int u; // u height of matrix B
	cl_ulong availableLocalMemory;
	cl_ulong neededLocalMemory;
	bool optimized;
public:
	struct WrapperArgs {
	  std::vector<float> inputVector0;
	  cl_int width0;
	  cl_int height0;
	  std::vector<float> inputVector1;
	  cl_int width1;
	  cl_int height1;
	  std::vector<float> outputVector;
	};
	std::vector<float> runMatrixVectorMultiOnePerProduct(const std::vector<float>& matrix, int rows, int cols, const
	std::vector<float>& vector);
	std::vector<float> runMatrixVectorMultiNPerProduct(const std::vector<float>& matrix, int rows, int cols, const
	std::vector<float>& vector);
	int runVectorAdd();
	explicit OpenCLWrapper(bool optimized = true);
private:
	int init();
	int setWorkGroupSize();
	cl::Program loadProgram(const std::string& fileName);
};

#endif //AMDMLOPENCL_OPENCLWRAPPER_HPP
