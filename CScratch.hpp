//
// Created by finn on 07.03.23.
//

#ifndef AMDMLOPENCL_CSCRATCH_HPP
#define AMDMLOPENCL_CSCRATCH_HPP

#include <CL/opencl.hpp>
#include <cstdio>
#include <iostream>
#include <eigen3/Eigen/Dense>
#include "BasicNeuralNetwork.hpp"
#include "OpenCLWrapper.hpp"

using Eigen::MatrixXf;
using Eigen::VectorXf;
namespace scratch {
	int scratchOpenCL();
	int scratch();
}

#endif //AMDMLOPENCL_CSCRATCH_HPP