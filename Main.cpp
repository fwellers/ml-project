//#define __CL_ENABLE_EXCEPTIONS
#define CL_HPP_ENABLE_EXCEPTIONS
#include <CL/opencl.hpp>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <random>
#include "CScratch.hpp"

int vector_add()
{
	try {
		// create device
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		if (platforms.empty()) {
			std::cerr << "No platforms found!" << std::endl;
			exit(1);
		}
		cl::Platform platform;
		for (auto &p : platforms) {
			std::string platformVersion = p.getInfo<CL_PLATFORM_VERSION>();
			if (platformVersion.find("OpenCL 2.") != std::string::npos ||
					platformVersion.find("OpenCL 3.") != std::string::npos) {
				// Note: an OpenCL 3.x platform may not support all required features!
				platform = p;
			}
		}
		if (platform() == nullptr) {
			std::cout << "No OpenCL 2.0 or newer platform found.\n";
			return -1;
		}
		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

		if (devices.empty()) {
			std::cerr << "No devices found!" << std::endl;
			exit(1);
		}
		cl::Device device = devices.front();
		cl::Context context = cl::Context(device);

		// load program
		std::ifstream kernel_file("cl/vector_addition.cl");
		std::string src(std::istreambuf_iterator<char>(kernel_file), (std::istreambuf_iterator<char>()));

		cl::Program::Sources sources;
		sources.emplace_back(src.c_str(), src.length());
		cl::Program program = cl::Program(context, sources);

		auto err = program.build();
		if (err!=CL_BUILD_SUCCESS) {
			std::cerr << "Error!\nBuild Status: " << program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device)
					  << "\nBuild Log:\t " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
			exit(1);
		}
		else {
			std::cout << "Built Successfully" << std::endl;
		}

		int ARRAY_DIM = 500000;
		std::vector<float> h_a(ARRAY_DIM);
		std::vector<float> h_b(ARRAY_DIM);
		std::vector<float> h_c(ARRAY_DIM);

		std::random_device rd;
		std::mt19937 g(rd());
		std::uniform_real_distribution<float> dist{-10000.0f, 10000.0f};
		for (int i = 0; i<ARRAY_DIM; i++) {
			h_a[i] = dist(g);
			h_b[i] = dist(g);
		}

		cl::Buffer d_a(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, ARRAY_DIM*sizeof(int),
				h_a.data());
		cl::Buffer d_b(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, ARRAY_DIM*sizeof(int),
				h_b.data());
		cl::Buffer d_c(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, ARRAY_DIM*sizeof(int));

		cl::Kernel kernel(program, "vadd");
		kernel.setArg(0, d_a);
		kernel.setArg(1, d_b);
		kernel.setArg(2, d_c);

		size_t globalWorkGroupSize = ARRAY_DIM/256;
		size_t localWorkGroupSize = 256;

		clock_t start, end;
		double diffOpencl = 0;
		int numExecutions = 400;
		cl::CommandQueue commandQueue(context, device);
		start = clock();
		for (int i = 0; i<numExecutions; i++) {
			commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(globalWorkGroupSize), cl::NDRange
					(localWorkGroupSize));
			commandQueue.enqueueReadBuffer(d_c, CL_TRUE, 0, ARRAY_DIM*sizeof(int), h_c.data());
			commandQueue.finish();
		}
		end = clock();
		diffOpencl += ((double) (end-start))/CLOCKS_PER_SEC;
		diffOpencl = 10e3 * diffOpencl/numExecutions;

		double diffCpu = 0;
		start = clock();
		for (int i = 0; i<numExecutions; i++) {
			for (int j = 0; j<ARRAY_DIM; j++) {
				h_c[j] = h_a[j]+h_b[j];
			}
		}
		end = clock();
		diffCpu += ((double) (end-start))/CLOCKS_PER_SEC;
		diffCpu = 10e3 * diffCpu/numExecutions;
		printf("The Array has %i entries.\n", ARRAY_DIM);
		printf("The duration for OpenCL was %f ms.\n", diffOpencl);
		printf("The duration for CPU was %f ms.\n", diffCpu);
		printf("The performance gain is %f percent.\n", (100*(diffCpu-diffOpencl)/diffOpencl));
		return 0;
	}
	catch (cl::Error& error) {
		std::cout << error.what() << " failed with error code " << error.err() << std::endl;
		return 1;
	}
}

int main()
{
	return scratch::scratch();
	//return vector_add();
}