#define ROW_DIM 0
#define COL_DIM 1
// work_table has N columns and get_local_size(0) rows.
__kernel void matrixVectorMult(__global const float* A,
		__global const float* X,
		__global float* C,
		__local float* work_table,
		int m, int n)
{
	// Compute partial dot product
	float sum =  0;
	for (int k = get_global_id(COL_DIM); k<n; k += get_global_size(COL_DIM)) {
		sum += A[get_global_id(ROW_DIM)+m*k]*X[k];
	}

	// Each thread stores its partial sum in work_table
	int rows = get_local_size(ROW_DIM); // rows in group
	int cols = get_local_size(COL_DIM); // initial cols in group
	int ii = get_local_id(ROW_DIM); // local row index in group, 0<=ii<rows
	int jj = get_local_id(COL_DIM); // block index in column, 0<=jj<cols
	work_table[ii+rows*jj] = sum;
	barrier(CLK_LOCAL_MEM_FENCE); // sync group

	// Reduce sums in log2(cols) steps
	while (cols>1) {
		cols >>= 1;
		if (jj<cols) work_table[ii+rows*jj] += work_table[ii+rows*(jj+cols)];
		barrier(CLK_LOCAL_MEM_FENCE); // sync group
	}

	// Write final result in C
	if (jj==0) C[get_global_id(ROW_DIM)] = work_table[ii];
}
