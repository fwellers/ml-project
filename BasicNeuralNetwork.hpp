//
// Created by finn on 07.03.23.
//

#ifndef AMDMLOPENCL_BASICNEURALNETWORK_HPP
#define AMDMLOPENCL_BASICNEURALNETWORK_HPP

#include <iostream>
#include <utility>
#include <vector>
#include <eigen3/Eigen/Dense>
#include <map>

#include "MathUtils.hpp"
#include "OpenCLWrapper.hpp"

using Eigen::MatrixXf;
using Eigen::VectorXf;

enum ActivationFunction {
  SIGMOID, ReLU, IDENTITY
};
class Layer {
	ActivationFunction activationFunction;
	int neurons;
};
class BasicNeuralNetwork {
	bool useGpu;
	OpenCLWrapper openClWrapper;
	const float learningRate = 0.001;
	std::vector<MatrixXf> weights;
	std::vector<VectorXf> outputVectors;
	std::vector<VectorXf> errorVectors;
	std::vector<VectorXf> networkValues;

public:
	void train(std::map<int, std::vector<std::array<unsigned char, 28*28>>> trainingsData);
	MatrixXf forwardPass(const VectorXf& inputVector);
	MatrixXf oldForwardPass(float value1, float value2);
	int backwardPassOnline(const VectorXf& targetValues);
    MatrixXf backwardPassBatch(VectorXf targetValues);
    MatrixXf backwardPassMiniBatch(VectorXf targetValues);
	void update(int layer, VectorXf errorVector);
	explicit BasicNeuralNetwork(bool useGpu = true);
private:
    VectorXf calcInitialErrors(VectorXf targetValues, VectorXf outputLayerValues);
    VectorXf calcError(VectorXf targetValues, VectorXf outputLayerValues);
	VectorXf multiplyMatrixVector(const MatrixXf& matrix, const VectorXf& vector);
};

#endif //AMDMLOPENCL_BASICNEURALNETWORK_HPP
