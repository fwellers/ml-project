__kernel void matrixVectorMult(__global const float *A, __global const float *X, __global float *C, int m, int n)
{
	float sum = 0.0f;
	int i = get_global_id(0); // row index
	for (int k=0;k<n;k++)
	{
		sum += A[i + m*k] * X[k];
	}
	C[i] = sum;
}
