//
// Created by finn on 07.03.23.
//

#ifndef AMMDLOPENCL_MATHUTILS_HPP
#define AMMDLOPENCL_MATHUTILS_HPP

#include <cmath>
#include <eigen3/Eigen/Dense>

namespace MathUtils {
	float softMax(float value, const Eigen::VectorXf& outputNeurons);
	float softMaxDeriv(float value, const Eigen::VectorXf& outputNeurons);
	float identity(float value);
	float reLU(float value);
	float reLUDeriv(float value);
	float sigmoid(float value);
	float sigmoidDeriv(float value);
	Eigen::MatrixXf matrixAdd(const Eigen::MatrixXf& matrix, const Eigen::VectorXf& vector);
	float calcError();
	float sumSquaredErrors(const Eigen::VectorXf& targetValues, Eigen::VectorXf& actualValues);
	std::vector<float> flattenMatrix(Eigen::MatrixXf matrix);
	std::vector<float> multiplyMatrixVector(const std::vector<float>& matrix, int rows, int cols, const
std::vector<float>& vector);
};

#endif //AMMDLOPENCL_MATHUTILS_HPP
