//
// Created by finn on 07.03.23.
//
#include "MathUtils.hpp"

float MathUtils::softMax(float value, const Eigen::VectorXf& outputNeurons){
	float maxElement = 0;
	for (int i = 0; i<outputNeurons.size(); i++) {
		if (maxElement<outputNeurons[i]) {
			maxElement = outputNeurons[i];
		}
	}
	double sumNeurons = 0;
	for (int i = 0; i<outputNeurons.size(); i++) {
		sumNeurons += std::exp(outputNeurons[i] - maxElement);
	}
	return (float) std::exp(value - maxElement)/sumNeurons;
}

float MathUtils::softMaxDeriv(float value, const Eigen::VectorXf& outputNeurons){
	return softMax(value, outputNeurons)*(1-softMax(value, outputNeurons));
}

float MathUtils::identity(float value) {
	return value;
}
float MathUtils::reLU(float value)
{
	return value>=0 ? value : 0;
}
float MathUtils::reLUDeriv(float value)
{
	return value>=0 ? 1 : 0;
}
float MathUtils::sigmoid(float value)
{
	return 1/(1+std::exp(-value));
}
float MathUtils::sigmoidDeriv(float value)
{
	return sigmoid(value)*(1-sigmoid(value));
}
Eigen::MatrixXf MathUtils::matrixAdd(const Eigen::MatrixXf& matrix, const Eigen::VectorXf& vector)
{
	Eigen::MatrixXf newMatrix = Eigen::MatrixXf::Zero(matrix.rows(), matrix.cols());
	for (int i = 0; i<matrix.rows(); i++) {
		for (int j = 0; j<matrix.cols(); j++) {
			newMatrix(i, j) = matrix(i, j)+vector(j);
		}
	}
	return newMatrix;
}

float MathUtils::sumSquaredErrors(const Eigen::VectorXf& targetValues, Eigen::VectorXf& actualValues)
{
	assert(targetValues.size()==actualValues.size());
	auto size = targetValues.size();
	float errorsSum;
	for (int i = 0; i<size; i++) {
		errorsSum += std::pow(actualValues(i)-targetValues(i), 2.0f);
	}
	return errorsSum;
}

std::vector<float> MathUtils::flattenMatrix(Eigen::MatrixXf matrix)
{
	Eigen::VectorXf eigenVector = Eigen::VectorXf{matrix.reshaped()};
	std::vector<float> outputVector;
	outputVector.resize(eigenVector.size());
	Eigen::VectorXf::Map(&outputVector[0], eigenVector.size()) = eigenVector;
	return outputVector;
}
std::vector<float> MathUtils::multiplyMatrixVector(const std::vector<float>& matrix, int rows, int cols,
		const std::vector<float>& vector)
{
	std::vector<float> output(vector.size());
	for (int i = 0; i < cols; i++){
		for (int j = 0; j < 2; j++){
			for (int k = 0; k < 2; k++){
				output[i] = 3;
			}
		}
	}
}
