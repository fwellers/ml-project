//
// Created by finn on 04.03.23.
//
#include <map>
#include "CScratch.hpp"

void getMyPlatformInfo(cl_platform_id platformId, cl_platform_info platformInfo, char variable[128])
{
	cl_int CL_err = CL_SUCCESS;
	CL_err = clGetPlatformInfo(platformId,
			platformInfo,
			128*sizeof(char),
			variable,
			NULL);
	if (CL_SUCCESS!=CL_err) {
		printf("Reading platform properties error.");
		// handle error
	}
}

cl_device_id getMyDeviceId(cl_platform_id platformId)
{
	cl_device_id device;
	int err;

	err = clGetDeviceIDs(platformId, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
	if (err==CL_DEVICE_NOT_FOUND) {
		printf("No GPU was found, using CPU instead.");
		err = clGetDeviceIDs(platformId, CL_DEVICE_TYPE_CPU, 1, &device, NULL);
	}
	if (err<0) {
		perror("Couln`t access any devices");
		exit(1);
	}
	return device;
}

int scratch::scratchOpenCL()
{
	cl_device_id device;
	cl_context context;
	cl_program program;
	cl_kernel kernel;
	cl_command_queue queue;
	cl_int CL_err = CL_SUCCESS;
	cl_uint numPlatforms = 0;
	cl_platform_id* platformIds = NULL;
	char vendorName[128] = {0};
	char openclVersion[128] = {0};

	CL_err = clGetPlatformIDs(0, NULL, &numPlatforms);

	if (CL_err==CL_SUCCESS) {
		printf("%u platform(s) found\n", numPlatforms);
	}
	else {
		printf("clGetPlatformIDs(%i)\n", CL_err);
	}

	platformIds = (cl_platform_id*) malloc(
			sizeof(cl_platform_id)*numPlatforms);
	if (NULL==platformIds) {
		printf("platformids null");
		// handle error
	}
	CL_err = clGetPlatformIDs(numPlatforms, platformIds, NULL);
	if (CL_SUCCESS!=CL_err) {
		printf("retrieve platforms error");
		// handle error
	}
	for (cl_uint ui = 0; ui<numPlatforms; ++ui) {
		getMyPlatformInfo(platformIds[ui], CL_PLATFORM_VENDOR, vendorName);
		printf("vendor name: %s\n", vendorName);
		getMyPlatformInfo(platformIds[ui], CL_PLATFORM_VERSION, openclVersion);
		printf("opencl version: %s\n", openclVersion);
	}
	cl_platform_id firstPlatformId = platformIds[1];
	getMyPlatformInfo(firstPlatformId, CL_PLATFORM_VENDOR, vendorName);
	getMyPlatformInfo(firstPlatformId, CL_PLATFORM_VERSION, openclVersion);
	printf("choosing first device with name %s and version %s\n", vendorName, openclVersion);

	device = getMyDeviceId(firstPlatformId);

	cl_device_svm_capabilities caps;

	cl_int err = clGetDeviceInfo(
			device,
			CL_DEVICE_SVM_CAPABILITIES,
			sizeof(cl_device_svm_capabilities),
			&caps,
			0
	);
	printf("SVM capabilities %i.", caps);
	return 0;
}
std::vector<std::array<unsigned char, 28*28>> loadMnist(char number)
{
	std::string baseFilePath = "/home/finn/Documents/master/ml/mnistDataSet/seperatedMnist/data";
	baseFilePath.push_back(number);
	std::ifstream file(baseFilePath, std::ios::binary);
	std::vector<std::array<unsigned char, 28*28>> outputVector(1000);
	for (int i = 0; i<1000; i++) {
		if (file.is_open()) {
			char tmpChar[28*28] = {};
			file.getline(tmpChar, 28*28);
			std::array<unsigned char, 28*28> tmpUnsigned = {};
			for (int j = 0; j<18*28; j++) {
				tmpUnsigned[j] = (unsigned char) tmpChar[j];
			}
			outputVector[i] = (tmpUnsigned);
		}
	}
	return outputVector;
}

int nn(std::map<int, std::vector<std::array<unsigned char, 28*28>>> trainingsData)
{
	BasicNeuralNetwork nnCPU = BasicNeuralNetwork(false);
	nnCPU.train(trainingsData);
	BasicNeuralNetwork nnGpu = BasicNeuralNetwork(true);
	nnGpu.train(trainingsData);
	/*
	MatrixXf m = nnGpu.forwardPass(2,3);
	VectorXf targetValues = VectorXf(1);
	targetValues(0) = 1.8f;
	nnGpu.backwardPassOnline(targetValues);
	nnGpu.update();
	std::cout << m << std::endl;
	 */
	printf("cool");
	return 0;
}
int runOpenClWrapper()
{
	OpenCLWrapper openClWrapper = OpenCLWrapper(false);
	MatrixXf weights = MatrixXf::Random(512, 784);
	VectorXf myVector = VectorXf::Random(784);

	//MatrixXf weights = MatrixXf::Random(12, 24);
	//VectorXf myVector = VectorXf::Random(24).unaryExpr([](float val) { return val*10; });

	/*
	MatrixXf weights = MatrixXf(2,3);
	weights(0,0) = 1;
	weights(0,1) = -1;
	weights(0,2) = 2;
	weights(1,0) = 0;
	weights(1,1) = -3;
	weights(1,2) = 1;

	VectorXf myVector = VectorXf(3);
	myVector(0) = 2;
	myVector(1) = 1;
	myVector(2) = 0;
	*/

	std::vector<float> outputVector;
	outputVector.resize(myVector.size());
	Eigen::VectorXf::Map(&outputVector[0], myVector.size()) = myVector;
	//auto resultOpenCL = openClWrapper
	//		.runMatrixVectorMultiOnePerProduct(MathUtils::flattenMatrix(weights), weights.rows(), weights
	//						.cols(),
	//				outputVector);
	auto resultOpenCL = openClWrapper
			.runMatrixVectorMultiNPerProduct(MathUtils::flattenMatrix(weights), weights.rows(), weights
							.cols(),
					outputVector);
	size_t start = clock();
	auto resultEigen = weights*myVector;
	size_t end = clock();
	double diffCpu = 10e3*((double) (end-start))/CLOCKS_PER_SEC;
	printf("The duration for Eigen was %f ms\n", diffCpu);
	std::cout << "opencl" << std::endl;
	//std::cout << Eigen::Map<VectorXf>(&resultOpenCL[0], resultOpenCL.size()) << std::endl;
	std::cout << "eigen" << std::endl;
	//std::cout << resultEigen << std::endl;
	return 0;
}

int runCpp()
{
	MatrixXf weights = MatrixXf::Random(256, 784);
	VectorXf myVector = VectorXf::Random(784);
	auto stuff = weights*myVector;
	std::cout << stuff << std::endl;
	return 0;
}
int scratch::scratch()
{
	std::map<int, std::vector<std::array<unsigned char, 28*28>>> trainingsData{
			{0, loadMnist('0')},
			{1, loadMnist('1')},
			{2, loadMnist('2')},
			{3, loadMnist('3')},
			{4, loadMnist('4')},
			{5, loadMnist('5')},
			{6, loadMnist('6')},
			{7, loadMnist('7')},
			{8, loadMnist('8')},
			{9, loadMnist('9')},
	};
	nn(trainingsData);
	//runOpenClWrapper();
	return 0;
}